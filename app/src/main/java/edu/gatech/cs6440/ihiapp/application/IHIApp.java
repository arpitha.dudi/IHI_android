package edu.gatech.cs6440.ihiapp.application;

import android.app.Application;
import android.content.Context;

import edu.gatech.cs6440.ihiapp.utils.AppPreference;

/**
 * Created by arpithadudi on 4/12/18.
 */

public class IHIApp extends Application {
    private Context mContext;
    private AppPreference mPreference;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public Context getApplicationContext() {
        return mContext;
    }

    public AppPreference getPreference() {
        if (mPreference == null) {
            mPreference = new AppPreference();
        }
        return mPreference;
    }


}