package edu.gatech.cs6440.ihiapp.receivers;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;
import java.util.HashMap;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.activities.MedicationActivity;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.Medicine;

/**
 * Created by arpithadudi on 4/30/18.
 */

public class MedicationNotificationService extends IntentService {

    private NotificationManager mNotificationManager;
    private NotificationChannel mChannelName;
    private String mChannelId;

    public MedicationNotificationService() {
        super("MedicationNotificationService");
    }

    private void createChannel(){
        mChannelId = getResources().getString(R.string.channel_id);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if(mChannelName == null) {
                CharSequence name = getResources().getString(R.string.channel_name);
                int importance = NotificationManager.IMPORTANCE_HIGH;
                mChannelName = new NotificationChannel(mChannelId, name, importance);
            }
        }
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        createChannel();
        Integer id = intent.getIntExtra("ID", -1);
        if(id != -1){
            String title = getNotificationText(id);
            if(!title.isEmpty()){
                sendNotification(title,id);
            }
        }
    }

    private String getNotificationText(Integer id){
        String title = "";
        IHIApp mApp = (IHIApp) getApplicationContext();
        HashMap<Integer,Medicine> list = mApp.getPreference().getAlarmMap(this);

        Medicine med = list.get(id);
        if(med != null){
            title += list.get(id).getName();
        }
        return title;
    }

    private void sendNotification(String msg, Integer id) {
        mNotificationManager = ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannelName);
        }

        Intent intent = new Intent(this, MedicationActivity.class);
        intent.putExtra("ID",id);

        //get pending intent
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Create notification
        NotificationCompat.Builder alamNotificationBuilder =  new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.notify_title))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(msg)
                .setAutoCancel(true)
                .setChannelId(mChannelId);
        alamNotificationBuilder.setContentIntent(contentIntent);

        //notiy notification manager about new notification
        mNotificationManager.notify(id, alamNotificationBuilder.build());
    }
}
