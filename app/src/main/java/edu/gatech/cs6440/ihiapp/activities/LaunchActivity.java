package edu.gatech.cs6440.ihiapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.User;

/**
 * Created by arpithadudi on 4/12/18.
 */

public class LaunchActivity extends AppCompatActivity {

    private IHIApp mApp;
    private User mUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        mApp = (IHIApp) getApplicationContext();
        mUser = mApp.getPreference().getUser(LaunchActivity.this);

        if (mUser != null){
            startActivity(new Intent(LaunchActivity.this, MainActivity.class));
            finish();
        }else{
            startActivity(new Intent(LaunchActivity.this, SignInActivity.class));
            finish();
        }
    }
}