package edu.gatech.cs6440.ihiapp.models;

import java.io.Serializable;

/**
 * Created by arpithadudi on 4/21/18.
 */

public class Vital implements Serializable {
    String date = "";
    String heartRate = "0";
    String bloodPressure = "0";
    String bloodSugar = "0";
    String weight = "0";

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getBloodSugar() {
        return bloodSugar;
    }

    public void setBloodSugar(String bloodSugar) {
        this.bloodSugar = bloodSugar;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
