package edu.gatech.cs6440.ihiapp.server;

import android.content.Context;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by arpithadudi on 3/21/18.
 */

public class ServerInterface {
    private ServerResponseCallback mCallback = null;
    private String mRequest = null;
    private ServerResponseData mResponseData = new ServerResponseData();
    public static final int LOGIN_REQUIRED = 403;

    public ServerInterface(String request, ServerResponseCallback callback) {
        mRequest = request;
        mCallback = callback;
    }

//    public void execute(Context context, RequestParams httpParams) {
//        String responseString = "";
//        if (mRequest.equals("/getdetails.php?type=doctors")) {
//            responseString = "[{\"id\":\"102\",\"name\":\"James Bond\"},{\"id\":\"103\",\"name\":\"Dave Gupta\"}," +
//                    "{\"id\":\"106\",\"name\":\"David Gupta\"}]";
//        } else if(mRequest.contains("type=medicines")){
////            responseString = "[]";
//            responseString = "[{\"code\":\"1049260\",\"name\":\"Acetaminophen 400 MG\",\"dosage\":\"Once a day\",\"timing\":\"9:00 AM\"}," +
//                    "{\"code\":\"1232194\",\"name\":\"Zolpidem tartrate 1.75 MG\",\"dosage\":\"Once a day\",\"timing\":\"9:00 AM\"}," +
//                    "{\"code\":\"197321\",\"name\":\"Alprazolam 1 MG\",\"dosage\":\"Once a day\",\"timing\":\"9:00 AM\"}]";
//        } else if(mRequest.contains("getVitals")){
//            responseString = "[]";
////            responseString = "[{\"weight\":\"50\",\"heartRate\":\"45\",\"bloodPressure\":\"60\",\"bloodSugar\":\"70\",\"date\":\"2018-04-29\"}]";
//        }
//
//        mResponseData.statusCode = ServerAPI.STATUS_OK;
//        mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
//        mResponseData.responseData = responseString;
//        if (mCallback != null) {
//            mCallback.onResponse(mResponseData);
//        }
//    }

    public void execute(Context context, RequestParams httpParams) {

        ServerAPI.get(context, mRequest, httpParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] response) {
                String responseString = null;
                try {
                    responseString = new String(response, "UTF-8");
                    if (statusCode == ServerAPI.STATUS_OK){
                        mResponseData.statusCode = ServerAPI.STATUS_OK;
                    } else {
                        mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    }
                    mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
                    mResponseData.responseData = responseString;

                } catch (UnsupportedEncodingException e) {
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String responseString = null;
                try {
                    responseString = new String(responseBody, "UTF-8");
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (mCallback != null) {
                    mCallback.onResponse(mResponseData);
                }
            }
        });
    }

    public void executeUrl(Context context, RequestParams httpParams) {

        ServerAPI.getAbsoulte(context, mRequest, httpParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] response) {
                String responseString = null;
                try {
                    responseString = new String(response, "UTF-8");
                    if (statusCode == ServerAPI.STATUS_OK){
                        mResponseData.statusCode = ServerAPI.STATUS_OK;
                    } else {
                        mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    }
                    mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
                    mResponseData.responseData = responseString;

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String responseString = null;
                try {
                    if(responseBody ==  null){
                        responseString = "";
                    } else {
                        responseString = new String(responseBody, "UTF-8");
                    }
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (mCallback != null) {
                    mCallback.onResponse(mResponseData);
                }
            }
        });
    }

    public void executePost(Context context, RequestParams httpParams) {

        ServerAPI.post(context, mRequest, httpParams, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] response) {
                String responseString = null;
                try {
                    responseString = new String(response, "UTF-8");
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String responseString = null;
                try {
                    responseString = new String(responseBody, "UTF-8");
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                if (mCallback != null) {
                    mCallback.onResponse(mResponseData);
                }
            }
        });
    }

//    public void executePostJSON(Context context, JSONObject data) {
//        String responseString = "";
//        if (mRequest.equals("/login.php")) {
//            responseString = "{\"id\":\"2739265\",\"name\":\"Arpitha\",\"email\":\"arpitha.dudi@gmail.com\",\"password\":\"94d96d6e3fe6914bd1c7451236173dde\",\"number\":\"9886563056\"}\n";
////            String responseString = "{\"message\":\"Authentication failed\"}";
//
//        } else if (mRequest.equals("/register.php")) {
//            responseString = "{\"message\":\"Account created successfully\"}";
////            responseString = "{\"message\":\"The email id already exists. Please enter a different one\"}";
//        } else if(mRequest.contains("updatemedication")){
//            responseString = "{\"message\":\"Medications where updated successfully.\"}";
//        } else if(mRequest.contains("updatemedication")){
//            responseString = " {\"message\":\"Medications where updated successfully.\"}";
//        } else if (mRequest.contains("updatevitals")){
//            responseString = " {\"message\":\"Vitals are updated successfully.\"}";
//        }
//        mResponseData.statusCode = ServerAPI.STATUS_OK;
//        mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
//        mResponseData.responseData = responseString;
//
//        if (mCallback != null) {
//            mCallback.onResponse(mResponseData);
//        }
//    }

    public void executePostJSON(Context context, JSONObject data) {
        ServerAPI.postJSON(context, mRequest, data, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] response) {
                String responseString = null;
                try {
                    responseString = new String(response, "UTF-8");
                    if (statusCode == ServerAPI.STATUS_OK){
                        mResponseData.statusCode = ServerAPI.STATUS_OK;
                    } else {
                        mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    }
                    mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
                    mResponseData.responseData = responseString;

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = "";
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String responseString = null;

                try {
                    responseString = new String(responseBody, "UTF-8");
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = "";
                }
            }

            @Override
            public void onFinish() {
                if (mCallback != null) {
                    mCallback.onResponse(mResponseData);
                }
            }
        });
    }

    public void executePostJSONArray(Context context, JSONArray data) {
        ServerAPI.postJSONArray(context, mRequest, data, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] response) {
                String responseString = null;
                try {
                    responseString = new String(response, "UTF-8");
                    if (statusCode == ServerAPI.STATUS_OK){
                        mResponseData.statusCode = ServerAPI.STATUS_OK;
                    } else {
                        mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    }
                    mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
                    mResponseData.responseData = responseString;

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = "";
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String responseString = null;

                try {
                    responseString = new String(responseBody, "UTF-8");
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = "";
                }
            }

            @Override
            public void onFinish() {
                if (mCallback != null) {
                    mCallback.onResponse(mResponseData);
                }
            }
        });
    }

    public void executeDelete(Context context, RequestParams httpParams) {

        ServerAPI.delete(context, mRequest, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String responseString = null;
                try {
                    responseString = new String(responseBody, "UTF-8");
                    if (statusCode == ServerAPI.STATUS_OK){
                        mResponseData.statusCode = ServerAPI.STATUS_OK;
                    } else {
                        mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    }
                    mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
                    mResponseData.responseData = responseString;

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String responseString = null;
                try {
                    responseString = new String(responseBody, "UTF-8");
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (mCallback != null) {
                    mCallback.onResponse(mResponseData);
                }
            }
        });
    }

    public void executePut(Context context, RequestParams httpParams) {

        ServerAPI.delete(context, mRequest, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String responseString = null;
                try {
                    responseString = new String(responseBody, "UTF-8");
                    if (statusCode == ServerAPI.STATUS_OK){
                        mResponseData.statusCode = ServerAPI.STATUS_OK;
                    } else {
                        mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    }
                    mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
                    mResponseData.responseData = responseString;

                } catch (UnsupportedEncodingException e) {
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String responseString = null;
                try {
                    responseString = new String(responseBody, "UTF-8");
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (mCallback != null) {
                    mCallback.onResponse(mResponseData);
                }
            }
        });
    }

    public void executePutJSON(Context context, JSONObject data) {

        ServerAPI.putJSON(context, mRequest,data, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String responseString = null;
                try {
                    responseString = new String(responseBody, "UTF-8");
                    if (statusCode == ServerAPI.STATUS_OK){
                        mResponseData.statusCode = ServerAPI.STATUS_OK;
                    } else {
                        mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    }
                    mResponseData.statusMsg = ServerAPI.STATUS_MSG_OK;
                    mResponseData.responseData = responseString;

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String responseString = null;
                try {
                    responseString = new String(responseBody, "UTF-8");
                    mResponseData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
                    mResponseData.responseData = responseString;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (mCallback != null) {
                    mCallback.onResponse(mResponseData);
                }
            }
        });
    }
}

