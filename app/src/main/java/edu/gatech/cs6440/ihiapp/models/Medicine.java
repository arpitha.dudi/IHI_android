package edu.gatech.cs6440.ihiapp.models;

import java.io.Serializable;
import java.util.ArrayList;

import edu.gatech.cs6440.ihiapp.R;

/**
 * Created by arpithadudi on 4/19/18.
 */

public class Medicine implements Serializable {

    String id = "";
    String name = "";
    ArrayList<String> timings = new ArrayList<>();
    String dosage = "Once a day";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getTimings() {
        return timings;
    }

    public void setTimings(ArrayList<String> timings) {
        this.timings = timings;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
