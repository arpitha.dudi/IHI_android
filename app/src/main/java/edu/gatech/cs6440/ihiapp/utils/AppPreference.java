package edu.gatech.cs6440.ihiapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

import edu.gatech.cs6440.ihiapp.models.Medicine;
import edu.gatech.cs6440.ihiapp.models.User;

/**
 * Created by arpithadudi on 4/12/18.
 */

public class AppPreference {

    private final String PREF_USER = "edu.gatech.cs6440.ihiapp.user";
    private final String PREF_DOC = "edu.gatech.cs6440.ihiapp.doc";
    private final String PREF_ALARM = "edu.gatech.cs6440.ihiapp.alarm";
    private final String PREF_MED = "edu.gatech.cs6440.ihiapp.med";

    //This function is used to clear all the preferences.
    public void clearAll(Context context) {
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor oEditor = oSharedPreference.edit();
        oEditor.clear();
        oEditor.commit();
    }

    private void setPreference(Context context, String name, boolean value) {
        if (context == null)
            return;
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor oEditor = oSharedPreference.edit();
        oEditor.putBoolean(name, value);
        oEditor.commit();
    }

    private boolean getPreference(Context context, String name, boolean defaultValue) {
        if (context == null)
            return defaultValue;
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        return oSharedPreference.getBoolean(name, defaultValue);
    }

    private void setPreference(Context context, String name, String value) {
        if (context == null)
            return;
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor oEditor = oSharedPreference.edit();
        oEditor.putString(name, value);
        oEditor.commit();
    }

    private String getPreference(Context context, String name, String defaultValue) {
        if (context == null)
            return defaultValue;
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        return oSharedPreference.getString(name, defaultValue);
    }

    private void setPreference(Context context, String name, int value) {
        if (context == null)
            return;
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor oEditor = oSharedPreference.edit();
        oEditor.putInt(name, value);
        oEditor.commit();
    }

    private int getPreference(Context context, String name, int defaultValue) {
        if (context == null)
            return defaultValue;
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        return oSharedPreference.getInt(name, defaultValue);
    }

    private void setPreference(Context context, String name, long value) {
        if (context == null)
            return;
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor oEditor = oSharedPreference.edit();
        oEditor.putLong(name, value);
        oEditor.commit();
    }

    private long getPreference(Context context, String name, long defaultValue) {
        if (context == null)
            return defaultValue;
        SharedPreferences oSharedPreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_MULTI_PROCESS);
        return oSharedPreference.getLong(name, defaultValue);
    }

    public void storeUser(Context context, User user) {
        if (context == null)
            return;

        if (user != null){
            Gson gson = new Gson();
            String json = gson.toJson(user);
            setPreference(context, PREF_USER, json);
        } else {
            setPreference(context, PREF_USER, "");
        }
    }

    public User getUser(Context context) {
        if (context == null)
            return null;

        User user = null;
        String json = getPreference(context, PREF_USER, "");
        if (!json.isEmpty()){
            Gson gson = new Gson();
            user = gson.fromJson(json, User.class);
        }

        return user;
    }

    public void storeDoctor(Context context, String id) {
        setPreference(context, PREF_DOC, id);
    }

    public String getDoctor(Context context) {
        return getPreference(context, PREF_DOC, "-1");
    }

    public void storeAlarmMap(Context context, HashMap<Integer,Medicine> map){
        if (context == null)
            return;

        if (map != null){
            Gson gson = new Gson();
            String json = gson.toJson(map);
            setPreference(context, PREF_ALARM, json);
        } else {
            setPreference(context, PREF_ALARM, "");
        }
    }

    public HashMap<Integer,Medicine> getAlarmMap(Context context){
        if (context == null)
            return null;

        HashMap<Integer,Medicine> list = null;
        String json = getPreference(context, PREF_ALARM, "");

        if (!json.isEmpty()){
            Gson gson = new Gson();
            java.lang.reflect.Type type = new TypeToken<HashMap<Integer,Medicine>>(){}.getType();
            list = gson.fromJson(json, type);
        }

        if(list == null){
            list = new HashMap<>();
        }
        return list;
    }

    public void storeMedMap(Context context, HashMap<String,ArrayList<Integer>> map){
        if (context == null)
            return;

        if (map != null){
            Gson gson = new Gson();
            String json = gson.toJson(map);
            setPreference(context, PREF_MED, json);
        } else {
            setPreference(context, PREF_MED, "");
        }
    }

    public HashMap<String,ArrayList<Integer>> getMedMap(Context context){
        if (context == null)
            return null;

        HashMap<String,ArrayList<Integer>> list = null;
        String json = getPreference(context, PREF_MED, "");

        if (!json.isEmpty()){
            Gson gson = new Gson();
            java.lang.reflect.Type type = new TypeToken<HashMap<String,ArrayList<Integer>>>(){}.getType();
            list = gson.fromJson(json, type);
        }

        return list;
    }
}
