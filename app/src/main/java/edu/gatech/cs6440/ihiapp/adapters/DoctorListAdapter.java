package edu.gatech.cs6440.ihiapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.User;

/**
 * Created by arpithadudi on 4/19/18.
 */

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.ViewHolder> {

    private List<User> mDataList;
    private Context mContext;
    private IHIApp mApp;
    private int selectedPosition = -1;
    private DoctorListAdapter.DoctorListAdapterListener mListener;

    public DoctorListAdapter(Context context, DoctorListAdapter.DoctorListAdapterListener listener) {
        mContext = context;
        mDataList = new ArrayList<User>();
        mApp = (IHIApp) mContext.getApplicationContext();
        mListener = listener;
    }

    public void addItems(ArrayList<User> list) {
        mDataList.clear();
        mDataList.addAll(list);
    }

    @Override
    public DoctorListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.doctor_list_item, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView);
    }

    public void onBindViewHolder(final DoctorListAdapter.ViewHolder holder, final int position) {
        User user = mDataList.get(position);
        holder.mDocName.setText(user.getName());
        holder.mCheckBox.setTag(position);

        holder.mCheckBox.setChecked(selectedPosition == position);
        holder.mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                notifyItemChanged(selectedPosition);
                selectedPosition = position;
                mListener.OnCheckClick(mDataList.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mDocName;
        private CheckBox mCheckBox;

        public ViewHolder(final View itemView) {
            super(itemView);
            mDocName = itemView.findViewById(R.id.name_view);
            mCheckBox = itemView.findViewById(R.id.check_view);
        }
    }

    public interface DoctorListAdapterListener {
        void OnCheckClick(User user);
    }
}