package edu.gatech.cs6440.ihiapp.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.adapters.MedTimeListAdapter;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.Medicine;
import edu.gatech.cs6440.ihiapp.models.User;
import edu.gatech.cs6440.ihiapp.server.ServerAPI;
import edu.gatech.cs6440.ihiapp.server.ServerResponseCallback;
import edu.gatech.cs6440.ihiapp.server.ServerResponseData;
import edu.gatech.cs6440.ihiapp.utils.Utils;

public class AddOrEditMedicationActivity extends BaseActivity implements MedTimeListAdapter.TimeListener, AdapterView.OnItemSelectedListener {

    // UI references.
    private EditText mMedNameView;
    private Spinner mDosageSpinner;
    private RecyclerView  mRecyclerView;

    //Variables
    private IHIApp mApp;
    private User mUser;
    private Utils mUtils;
    private int mIndex = -1;
    private boolean isSaved = false;
    private Medicine mMedication;
    private MedTimeListAdapter mAdapter;
    private ProgressDialog mProgressWithText;

    @Override
    public void onBackPressed() {
        onBackClicked();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_medication);

        mApp = (IHIApp) getApplicationContext();
        mUser = mApp.getPreference().getUser(AddOrEditMedicationActivity.this);

        mUtils = new Utils();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if(intent != null){
            if(intent.hasExtra("MED")){
                mMedication = (Medicine) intent.getSerializableExtra("MED");
                mIndex = intent.getIntExtra("INDEX",-1);
            } else {
                return;
            }
        }

        mMedNameView = findViewById(R.id.name_view);
        mMedNameView.setEnabled(false);
        mDosageSpinner = findViewById(R.id.dosage);
        mRecyclerView = findViewById(R.id.time_list);

        mMedNameView.setText(mMedication.getName());
        mApp.getPreference().storeAlarmMap(this,null);
        setupSpinner();
        setupListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.save_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackClicked();
                return true;
            case R.id.action_settings:
                onSaveClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupListView() {
        mAdapter = new MedTimeListAdapter(AddOrEditMedicationActivity.this,AddOrEditMedicationActivity.this);
        mAdapter.addItem(mMedication.getTimings());

        //setup the recyclerview
        mRecyclerView.setLayoutManager(new LinearLayoutManager(AddOrEditMedicationActivity.this));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupSpinner() {
        final String[] list = getResources().getStringArray(R.array.dosage_array);

        mDosageSpinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> array_list = new ArrayAdapter<>(AddOrEditMedicationActivity.this,android.R.layout.simple_spinner_item,list);
        array_list.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDosageSpinner.setAdapter(array_list);

        if (mMedication.getDosage().equals(getString(R.string.once_day))){
            mDosageSpinner.setSelection(0);
        } else if (mMedication.getDosage().equals(getString(R.string.twice_day))){
            mDosageSpinner.setSelection(1);
        } else {
            mDosageSpinner.setSelection(2);
        }
    }

    private void onBackClicked(){
        Intent returnIntent = new Intent();
        if(isSaved){
            returnIntent.putExtra("MED", mMedication);
            returnIntent.putExtra("INDEX", mIndex);
        }
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    private void onSaveClicked(){
        showProgressWithText(getString(R.string.update_progress));
        updateMedication();
    }

    @Override
    public void OnClick(int position) {
        openPicker(position);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        mMedication.setDosage(mDosageSpinner.getSelectedItem().toString());
        ArrayList<String> list = mMedication.getTimings();
        if (position ==  0){
            if(list.size() > 2){
                list.remove(2);
                list.remove(1);
            } else if(list.size() > 1) {
                list.remove(1);
            }
            mMedication.setTimings(list);
            mAdapter.addItem(mMedication.getTimings());
            mAdapter.notifyDataSetChanged();
        } else if(position ==  1){
            if(list.size() == 1){
                list.add("3:30 PM");
            } else if(list.size() == 3){
                list.remove(2);
            }

            mMedication.setTimings(list);
            mAdapter.addItem(mMedication.getTimings());
            mAdapter.notifyDataSetChanged();
        }else {
            if(list.size() < 2){
                list.add("3:30 PM ");
            }
            if(list.size() != 3){
                list.add("8:00 PM");
            }

            mMedication.setTimings(list);
            mAdapter.addItem(mMedication.getTimings());
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void openPicker(final int position){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddOrEditMedicationActivity.this, AlertDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                boolean isPM = (selectedHour >= 12);
                int hourOfDay = (selectedHour == 12 || selectedHour == 0) ? 12 : selectedHour % 12;
                String zone = isPM ? "PM" : "AM";
                mAdapter.editItem(position,hourOfDay + ":" + selectedMinute + " " + zone);
                mAdapter.notifyDataSetChanged();
            }
        }, hour, minute, false);
        mTimePicker.setTitle("Set Time");
        mTimePicker.show();
    }


    public void updateMedication() {
        ServerAPI.updateMedication(mApp.getApplicationContext(), mMedication, mUser.getId(), new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response) {
                if (response.statusCode == ServerAPI.STATUS_OK) {
                    try {
                        JSONObject responseData = new JSONObject(response.responseData);
                        String message = responseData.getString("message");
                        if (message.contains("success")){
                            isSaved = true;
                            dismissProgressWithText();
                            showProgressWithText(getString(R.string.reminders_progress));
                            configureReminders();
                        } else {
                            // Show the error to the user
                            dismissProgress();
                            showErrorToast(AddOrEditMedicationActivity.this,getString(R.string.update_error));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        // Show the error to the user
                        dismissProgress();
                        showErrorToast(AddOrEditMedicationActivity.this,getString(R.string.update_error));
                    }


                } else {
                    // Show the error to the user
                    dismissProgress();
                    showErrorToast(AddOrEditMedicationActivity.this,getString(R.string.update_error));
                }
            }
        });
    }

    private void configureReminders(){
        ArrayList<String> time = mMedication.getTimings();
        ArrayList<Integer> newAlarmIds = new ArrayList<>();
        ArrayList<Integer> oldAlarmIds = new ArrayList<>();

        for(int i=0 ; i < time.size(); i ++){
            newAlarmIds.add(mUtils.getUniqueKey(mMedication.getId(), i));
        }

        HashMap<String,ArrayList<Integer>> mMedList = mApp.getPreference().getMedMap(AddOrEditMedicationActivity.this);
        if(mMedList != null  && mMedList.containsKey(mMedication.getId())){
            oldAlarmIds = mMedList.get(mMedication.getId());
        } else {
            mMedList = new HashMap<String,ArrayList<Integer>>();
        }

        mMedList.put(mMedication.getId(),newAlarmIds);
        mApp.getPreference().storeMedMap(AddOrEditMedicationActivity.this,mMedList);

        deleteAlarms(oldAlarmIds);
        updateAlarms(time);

        dismissProgressWithText();
    }

    private void updateAlarms(ArrayList<String> time){
        HashMap<Integer,Medicine> alarmList = mApp.getPreference().getAlarmMap(AddOrEditMedicationActivity.this);

        Medicine alarmMed = new Medicine();
        alarmMed.setId(mMedication.getId());
        alarmMed.setName(mMedication.getName());
        for(int i = 0 ; i < time.size(); i ++){
            int alarmId = mUtils.getUniqueKey(mMedication.getId(), i);
            String[] timeList = TextUtils.split(mUtils.get24HourFormat(time.get(i)),":");
            Log.i("IHI", "id:" + alarmId + timeList[0] + timeList[1]);
            mUtils.setAlarm(mApp,alarmId,timeList);
            alarmList.put(alarmId,alarmMed);
        }
        mApp.getPreference().storeAlarmMap(AddOrEditMedicationActivity.this,alarmList);
    }

    private void deleteAlarms(ArrayList<Integer> list){
        mUtils.removeAlarm(mApp,list);

        HashMap<Integer,Medicine> alarmList = mApp.getPreference().getAlarmMap(AddOrEditMedicationActivity.this);

        for (Integer id : list) {
            if(alarmList != null){
                alarmList.remove(id);
            }
        }
        mApp.getPreference().storeAlarmMap(AddOrEditMedicationActivity.this,alarmList);
    }

    private void showProgressWithText(String text) {
        mProgressWithText = new ProgressDialog(AddOrEditMedicationActivity.this);
        mProgressWithText.setMessage(text);
        mProgressWithText.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressWithText.setCancelable(false);
        mProgressWithText.setCanceledOnTouchOutside(true);
        mProgressWithText.setIndeterminate(true);
        mProgressWithText.show();
    }

    private void dismissProgressWithText(){
        if (mProgressWithText != null){
            mProgressWithText.dismiss();
        }
        mProgressWithText = null;
    }
}
