package edu.gatech.cs6440.ihiapp.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.User;
import edu.gatech.cs6440.ihiapp.models.Vital;
import edu.gatech.cs6440.ihiapp.server.ServerAPI;
import edu.gatech.cs6440.ihiapp.server.ServerResponseCallback;
import edu.gatech.cs6440.ihiapp.server.ServerResponseData;

public class AddVitalActivity extends BaseActivity {

    //UI Elements
    private EditText mVitalValView;
    private EditText mDateView;

    //Variables
    private IHIApp mApp;
    private User mUser;
    private String mVal;
    private String mDate;
    private String mType;
    private boolean isSaved;


    @Override
    public void onBackPressed() {
        onBackClicked();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vital);

        mApp = (IHIApp) getApplicationContext();
        mUser = mApp.getPreference().getUser(AddVitalActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if(intent != null){
            mType = intent.getStringExtra("TYPE");
        }

        getSupportActionBar().setTitle(mType);
        mVitalValView = findViewById(R.id.name_view);
        mVitalValView.setHint(mType);
        mDateView  = findViewById(R.id.date_view);
        mDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPicker();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.save_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackClicked();
                return true;
            case R.id.action_settings:
                onSaveClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openPicker(){
        Calendar mCurrentDate = Calendar.getInstance();
        int year = mCurrentDate.get(Calendar.YEAR);
        int month = mCurrentDate.get(Calendar.MONTH);
        int day = mCurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(AddVitalActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                new DatePickerDialog.OnDateSetListener(){

                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                        String date = dateFormatter.format(newDate.getTime());
                        mDateView.setText(date);
                        mDate = date;
                    }
                },year, month, day);

        mDatePicker.show();
    }

    private void onBackClicked(){
        Intent returnIntent = new Intent();
        if(isSaved){
            returnIntent.putExtra("VALUE", mVal);
            returnIntent.putExtra("DATE", mDate);
        }
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    private void onSaveClicked(){
        mVal = mVitalValView.getText().toString();
        showProgress();
        addVitals();
    }

    public void addVitals() {
        JSONObject data = getJSONObject();
        if (data == null){
            // Show the error to the user
            dismissProgress();
            showErrorToast(AddVitalActivity.this,getString(R.string.update_error));
            return;
        }

        ServerAPI.addVitals(mApp.getApplicationContext(),mUser.getId(),data, new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response) {
                if (response.statusCode == ServerAPI.STATUS_OK) {
                    if (response.responseData.contains("success")){
                        dismissProgress();
                        isSaved = true;
                    } else {
                        // Show the error to the user
                        dismissProgress();
                        showErrorToast(AddVitalActivity.this,getString(R.string.update_error));
                    }
                } else {
                    // Show the error to the user
                    dismissProgress();
                    showErrorToast(AddVitalActivity.this,getString(R.string.update_error));
                }
            }
        });
    }

    private JSONObject getJSONObject(){
        JSONObject vitalObj = null;
        try {
            vitalObj = new JSONObject();
            if(mType.contains("Weight")){
                vitalObj.put("weight", mVal);
            } else if (mType.contains("Rate")){
                vitalObj.put("heartRate", mVal);
            }else if (mType.contains("Pressure")){
                vitalObj.put("bloodPressure", mVal);
            }else if (mType.contains("Sugar")){
                vitalObj.put("bloodSugar", mVal);
            }
            vitalObj.put("date", mDate);
        }catch (JSONException e){

        }
        return  vitalObj;
    }

}
