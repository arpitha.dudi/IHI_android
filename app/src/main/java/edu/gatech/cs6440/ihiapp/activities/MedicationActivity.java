package edu.gatech.cs6440.ihiapp.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.adapters.MedicationListAdapter;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.Medicine;
import edu.gatech.cs6440.ihiapp.models.User;
import edu.gatech.cs6440.ihiapp.server.ServerAPI;
import edu.gatech.cs6440.ihiapp.server.ServerResponseCallback;
import edu.gatech.cs6440.ihiapp.server.ServerResponseData;

/**
 * Created by arpithadudi on 4/30/18.
 */

public class MedicationActivity extends BaseActivity implements MedicationListAdapter.MedicationListener{

    //Variables
    private IHIApp mApp;
    private User mUser;
    private Integer mMedEntryID;
    private MedicationListAdapter mAdapter;
    private Medicine mMedicine;
    //UI Elements
    private RecyclerView mRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medications);

        mApp = (IHIApp) getApplicationContext();
        mUser = mApp.getPreference().getUser(MedicationActivity.this);

        mMedEntryID = getIntent().getIntExtra("ID", -1);
        setupListView();
        setupData();
    }

    private void setupListView() {
        mAdapter = new MedicationListAdapter(MedicationActivity.this,MedicationActivity.this);

        //setup the recyclerview
        mRecyclerView = findViewById(R.id.medication_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MedicationActivity.this));
        mRecyclerView.setAdapter(mAdapter);
    }

    private  void setupData(){
        HashMap<Integer,Medicine> list = mApp.getPreference().getAlarmMap(this);
        ArrayList<Medicine> med_list = new ArrayList<>();
        mMedicine = list.get(mMedEntryID);
        if(mMedicine != null){
            med_list.add(list.get(mMedEntryID));
        }
        mAdapter.addItems(med_list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnClick(int position, boolean flag) {
        if(!flag){
            showProgress();
            try {
                JSONObject top = new JSONObject();
                top.put("code" , mMedicine.getId());
                top.put("count", "1");
                top.put("date", getDate());
                top.put("pid", mUser.getId());

                ServerAPI.updateMedicationLapse(MedicationActivity.this, top, new ServerResponseCallback() {
                    @Override
                    public void onResponse(ServerResponseData response) {
                        if (response.statusCode == ServerAPI.STATUS_OK) {
                            dismissProgress();
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String getDate(){
        Calendar mCurrentDate = Calendar.getInstance();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormatter.format(mCurrentDate.getTime());

        return date;
    }
}
