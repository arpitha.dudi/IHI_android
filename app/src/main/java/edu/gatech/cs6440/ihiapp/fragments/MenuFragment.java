package edu.gatech.cs6440.ihiapp.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.Medicine;
import edu.gatech.cs6440.ihiapp.receivers.AlarmReceiver;

/**
 * Created by arpithadudi on 4/19/18.
 */

public class MenuFragment extends Fragment {
    private static final String ARG_TEXT = "arg_text";
    private static final String ARG_COLOR = "arg_color";

    private String mText;
    private int mColor;

    //Variables
    private IHIApp mApp;
    private View mContent;
    private EditText mTextView;
    private Button mButton;
    private PendingIntent pendingIntent;
    private Button mCancel;

    public static Fragment newInstance(String text, int color) {
        Fragment frag = new MenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        args.putInt(ARG_COLOR, color);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mApp = (IHIApp) getActivity().getApplicationContext();

//        /* Retrieve a PendingIntent that will perform a broadcast */
//        Intent alarmIntent = new Intent(getActivity(), AlarmReceiver.class);
//        alarmIntent.putExtra("ARP", "Hello");
//        pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, 0);

        // retrieve text and color from bundle or savedInstanceState
//        if (savedInstanceState == null) {
//            Bundle args = getArguments();
//            mText = args.getString(ARG_TEXT);
//            mColor = args.getInt(ARG_COLOR);
//        } else {
//            mText = savedInstanceState.getString(ARG_TEXT);
//            mColor = savedInstanceState.getInt(ARG_COLOR);
//        }

        // initialize views
//        mContent = view.findViewById(R.id.fragment_content);
//        mTextView = view.findViewById(R.id.text);
//        mButton = view.findViewById(R.id.button);
//        mButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mText = mTextView.getText().toString();
////                String str = "9:00 AM";
////                str = str.replaceAll("\\D+","");
////                Log.d("ARP","Str" + str);
//                //startAlarm();
//
//            }
//        });
//
//        mCancel = view.findViewById(R.id.cancel);
//        mCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                cancelAlarm();
//            }
//        });


        // set text and background color
//        mTextView.setText(mText);
//        mContent.setBackgroundColor(mColor);
    }

    private void startAlarm(){
//        HashMap<String,Integer> timeMap =  new HashMap<String,Integer>();
//        timeMap.put("9:00 AM", 101);
//        mApp.getPreference().storeTimeMap(getActivity(),timeMap);
//
//        HashMap<Integer,ArrayList<Medicine>> map = new HashMap<>();
//        ArrayList<Medicine> list = new ArrayList<>();
//        Medicine med = new Medicine();
//        med.setId("123");
//        med.setName("Crocin Tablet");
//        list.add(med);
//        map.put(101,list);
//        mApp.getPreference().storeAlarmMap(getActivity(),map);

        String[] time = TextUtils.split(mText,":");
        AlarmManager manager = (AlarmManager) mApp.getSystemService(Context.ALARM_SERVICE);

        /* Set the alarm to start at 10:30 AM */
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));

        Intent serviceIntent = new Intent(mApp, AlarmReceiver.class);
        serviceIntent.setAction("edu.gatech.cs6440.ihiapp.action.ALARM");
        serviceIntent.putExtra("ID", 101);

        PendingIntent servicePendingIntent = PendingIntent.getBroadcast(mApp,101,
                        serviceIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        manager.setRepeating(AlarmManager.RTC_WAKEUP,//type of alarm. This one will wake up the device when it goes off, but there are others, check the docs
                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY,
                servicePendingIntent);

        //assert manager != null;
//        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), servicePendingIntent);

        /* Repeating on every  minutes interval */
//        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
//                1000 * 60 * 1, pendingIntent);
    }

    private void cancelAlarm(){
        AlarmManager manager = (AlarmManager) mApp.getSystemService(Context.ALARM_SERVICE);
        Intent cancelServiceIntent = new Intent(mApp, AlarmReceiver.class);
        cancelServiceIntent.setAction("edu.gatech.cs6440.ihiapp.action.ALARM");
        PendingIntent cancelServicePendingIntent = PendingIntent.getBroadcast(
                mApp,
                101, // integer constant used to identify the service
                cancelServiceIntent,
                0 //no FLAG needed for a service cancel
        );
        manager.cancel(cancelServicePendingIntent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(ARG_TEXT, mText);
        outState.putInt(ARG_COLOR, mColor);
        super.onSaveInstanceState(outState);
    }
}
