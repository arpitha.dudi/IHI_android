package edu.gatech.cs6440.ihiapp.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import static android.support.v4.content.WakefulBroadcastReceiver.startWakefulService;


/**
 * Created by arpithadudi on 4/30/18.
 */

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ComponentName comp = new ComponentName(context.getPackageName(), MedicationNotificationService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
    }
}
