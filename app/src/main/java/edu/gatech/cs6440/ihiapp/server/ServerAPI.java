package edu.gatech.cs6440.ihiapp.server;

/**
 * Created by arpithadudi on 3/26/18.
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.StringEntity;
import edu.gatech.cs6440.ihiapp.BuildConfig;
import edu.gatech.cs6440.ihiapp.models.Medicine;
import edu.gatech.cs6440.ihiapp.models.User;
import edu.gatech.cs6440.ihiapp.models.Vital;

public class ServerAPI {

    public static final String STATUS_MSG_OK = "OK";

    public static final int STATUS_OK = 200;
    public static final int STATUS_UNKNOWNERROR = -1;

    public static final String SERVER_BASE_URL = "https://cs6440-s18-prj53.apps.hdap.gatech.edu/patientservice";

    private static AsyncHttpClient client = null;
    private static PersistentCookieStore mCookieStore = null;
    private static ServerResponseData mResponseErrorData = new ServerResponseData();

    private ServerResponseCallback mCallback = null;

    public static void init(Context context) {
        client = null;
        client = new AsyncHttpClient();
        client.setUserAgent("Version " + BuildConfig.VERSION_NAME + " deviceID " + getDeviceId(context));
        if (mCookieStore == null) {
            mCookieStore = new PersistentCookieStore(context);
            client.setCookieStore(mCookieStore);
        }
    }

    private static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static void shutdown(Context context) {
        if (mCookieStore != null) {
            mCookieStore.clear();
            mCookieStore = null;
            mCookieStore = new PersistentCookieStore(context);
            client.setCookieStore(mCookieStore);
        }
    }

    public static PersistentCookieStore getCookieStore() {
        return mCookieStore;
    }

    public static void get(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        if (client == null)
            ServerAPI.init(context);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void getAbsoulte(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        if (client == null)
            ServerAPI.init(context);
        client.get(url, params, responseHandler);
    }

    public static void post(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        if (client == null)
            ServerAPI.init(context);
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void postJSON(Context context, String url, JSONObject data,
                                AsyncHttpResponseHandler responseHandler) {
        StringEntity entity;
        try {
            if (client == null) {
                ServerAPI.init(context);
            }
            entity = new StringEntity(data.toString());
            entity.setContentType("application/json");
            client.post(context, getAbsoluteUrl(url), entity, "application/json", responseHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void postJSONArray(Context context, String url, JSONArray data,
                                AsyncHttpResponseHandler responseHandler) {
        StringEntity entity;
        try {
            if (client == null) {
                ServerAPI.init(context);
            }
            entity = new StringEntity(data.toString());
            entity.setContentType("application/json");
            client.post(context, getAbsoluteUrl(url), entity, "application/json", responseHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void putJSON(Context context, String url, JSONObject data,
                                AsyncHttpResponseHandler responseHandler) {
        StringEntity entity;
        try {
            if (client == null) {
                ServerAPI.init(context);
            }
            entity = new StringEntity(data.toString());
            entity.setContentType("application/json");
            client.put(context, getAbsoluteUrl(url), entity, "application/json", responseHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void delete(Context context, String url, AsyncHttpResponseHandler responseHandler) {
        if (client == null)
            ServerAPI.init(context);
        client.delete(getAbsoluteUrl(url), responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return ServerAPI.SERVER_BASE_URL + relativeUrl;
    }

    public static void login(Context context, String id, String password, ServerResponseCallback callback) {
        try {
            String url = "/login.php";
            JSONObject top = new JSONObject();
            top.put("id", id);
            top.put("password", password);

            ServerInterface request = new ServerInterface(url, callback);
            request.executePostJSON(context, top);
        } catch (Exception e) {
            e.printStackTrace();
            // Error
            mResponseErrorData.statusCode = ServerAPI.STATUS_UNKNOWNERROR;
            mResponseErrorData.responseData = "";
        }
    }

    public static void createAccount(Context context, User user, String docId, ServerResponseCallback callback) {
        String url = "/register.php";

        try {
            JSONObject top = new JSONObject();
            top.put("id", user.getId());
            top.put("name", user.getName());
            top.put("email", user.getEmail());
            top.put("number", user.getNumber());
            top.put("password", user.getPassword());
            top.put("doc", docId);
            ServerInterface request = new ServerInterface(url, callback);
            request.executePostJSON(context, top);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void getDoctorsList(Context context, ServerResponseCallback callback) {
        String url = "/getdetails.php?type=doctors";
        ServerInterface request = new ServerInterface(url, callback);
        request.execute(context,null);
    }

    public static void getMedicationsFromLocal(Context context, String id, ServerResponseCallback callback) {
        String url = "/getdetails.php?type=medicines&pid=" + id;
        ServerInterface request = new ServerInterface(url, callback);
        request.execute(context,null);
    }

    public static void getMedications(Context context, String userID, ServerResponseCallback callback) {
        String url = "http://hapi.fhir.org/baseDstu3/MedicationRequest?patient=" + userID + "&_format=json";
        ServerInterface request = new ServerInterface(url, callback);
        request.executeUrl(context, null);
    }

    public static void updateMedicationsToLocal(Context context, JSONObject data, ServerResponseCallback callback) {
        String url = "/addmedications.php";
        ServerInterface request = new ServerInterface(url, callback);
        request.executePostJSON(context,data);
    }

    public static void updateMedication(Context context, Medicine med, String id, ServerResponseCallback callback) {
        String url = "/updatemedication.php";

        try {
            JSONObject top = new JSONObject();
            top.put("code", med.getId());
            top.put("pid", id);
            top.put("dosage", med.getDosage());
            top.put("timing", TextUtils.join(", ", med.getTimings()));
            ServerInterface request = new ServerInterface(url, callback);
            request.executePostJSON(context, top);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static void getVitals(Context context, String userID, ServerResponseCallback callback) {
        String url = "/getvitals.php?pid=" + userID;

        ServerInterface request = new ServerInterface(url, callback);
        request.execute(context, null);
    }

    public static void addVitals(Context context, String userID, JSONObject object, ServerResponseCallback callback) {
        String url = "/updatevitals.php";

        try {
            JSONObject top = new JSONObject();
            top.put("id", userID);
            JSONArray array = new JSONArray();
            array.put(object);
            top.put("list",array);
            ServerInterface request = new ServerInterface(url, callback);
            request.executePostJSON(context, top);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void updateMedicationLapse(Context context, JSONObject object, ServerResponseCallback callback) {
        String url = "/medicationlapse.php";
        JSONArray array = new JSONArray();
        array.put(object);
        ServerInterface request = new ServerInterface(url, callback);
        request.executePostJSONArray(context, array);
    }
}
