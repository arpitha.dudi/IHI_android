package edu.gatech.cs6440.ihiapp.server;

/**
 * Created by arpithadudi on 3/21/18.
 */

public interface ServerResponseCallback {
    void onResponse(ServerResponseData response);
}
