package edu.gatech.cs6440.ihiapp.models;

/**
 * Created by arpithadudi on 4/12/18.
 */

public class User {

    String id = "";
    String name = "";
    String email =  "";
    String number =  "";
    String password =  "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
