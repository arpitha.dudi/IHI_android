package edu.gatech.cs6440.ihiapp.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import edu.gatech.cs6440.ihiapp.receivers.AlarmReceiver;

/**
 * Created by arpithadudi on 4/30/18.
 */

public class Utils {

    public void setAlarm(Context context, int id, String[] time){

        Log.i("IHI", "id:" + id + time[0] + time[1]);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        /* Set the alarm to start at 10:30 AM */
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));

        Intent serviceIntent = new Intent(context, AlarmReceiver.class);
        serviceIntent.setAction("edu.gatech.cs6440.ihiapp.action.ALARM");
        serviceIntent.putExtra("ID", id);

        PendingIntent servicePendingIntent = PendingIntent.getBroadcast(context,id,
                serviceIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        manager.setRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY,
                servicePendingIntent);
    }

    public void removeAlarm(Context context, ArrayList<Integer> list){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent cancelServiceIntent = new Intent(context, AlarmReceiver.class);
        cancelServiceIntent.setAction("edu.gatech.cs6440.ihiapp.action.ALARM");

        if(manager != null) {
            for (Integer id : list) {
                PendingIntent cancelServicePendingIntent = PendingIntent.getBroadcast(
                        context, id, cancelServiceIntent, 0);
                manager.cancel(cancelServicePendingIntent);
            }
        }
    }

    public String get24HourFormat(String text){
        String formated = text;
        try {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            Date date = parseFormat.parse(text);
            formated = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formated;
    }

    public int getUniqueKey(String id , int index){
        return Integer.valueOf(id + index);
    }


}
