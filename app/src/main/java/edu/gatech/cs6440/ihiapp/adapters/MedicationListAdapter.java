package edu.gatech.cs6440.ihiapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.models.Medicine;

/**
 * Created by arpithadudi on 4/30/18.
 */

public class MedicationListAdapter extends RecyclerView.Adapter<MedicationListAdapter.ViewHolder> {

    private Context mContext;
    private List<Medicine> mDataList;
    private MedicationListener mListener;

    public MedicationListAdapter(Context context, MedicationListener listener) {
        mContext = context;
        mDataList = new ArrayList<Medicine>();
        mListener = listener;
    }

    public void addItems(ArrayList<Medicine> list) {
        mDataList.clear();
        mDataList.addAll(list);
    }

    public Medicine editItem(int index, Medicine med) {
        return mDataList.set(index, med);
    }

    public Medicine getItems(int index) {
        return mDataList.get(index);
    }

    @Override
    public MedicationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.medication_list_item, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(MedicationListAdapter.ViewHolder holder, final int position) {
        Medicine med = mDataList.get(position);
        holder.mHeader.setText("Medication " + (position+1));
        holder.mName.setText(med.getName());

        holder.mSkipBtn.setTag(position);
        holder.mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = (int) view.getTag();
                mListener.OnClick(position, false);
            }
        });

        holder.mTakeBtn.setTag(position);
        holder.mTakeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = (int) view.getTag();
                mListener.OnClick(position, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mHeader;
        private TextView mName;
        private Button mSkipBtn;
        private Button mTakeBtn;

        public ViewHolder(final View itemView) {
            super(itemView);
            mHeader = itemView.findViewById(R.id.header_view);
            mName = itemView.findViewById(R.id.name_view);
            mSkipBtn = itemView.findViewById(R.id.btn_skip);
            mTakeBtn = itemView.findViewById(R.id.btn_take);
        }
    }

    public interface MedicationListener {
        void OnClick(int position , boolean flag);
    }
}