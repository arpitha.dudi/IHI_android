package edu.gatech.cs6440.ihiapp.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.activities.AddOrEditMedicationActivity;
import edu.gatech.cs6440.ihiapp.adapters.MedicineListAdapter;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.Medicine;
import edu.gatech.cs6440.ihiapp.models.User;
import edu.gatech.cs6440.ihiapp.server.ServerAPI;
import edu.gatech.cs6440.ihiapp.server.ServerResponseCallback;
import edu.gatech.cs6440.ihiapp.server.ServerResponseData;
import edu.gatech.cs6440.ihiapp.utils.Utils;

/**
 * Created by arpithadudi on 4/19/18.
 */

public class MedicationFragment extends Fragment implements MedicineListAdapter.MedicineListener {

    //Variables
    private IHIApp mApp;
    private User mUser;
    private Utils mUtils;

    // UI references
    private MedicineListAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medication, container, false);

        mApp = (IHIApp) getActivity().getApplicationContext();
        mUser = mApp.getPreference().getUser(getActivity());
        mUtils = new Utils();

        //setup the recyclerview
        setupListView(view);
        checkAndFetchMedications();
        return view;
    }

    private void checkAndFetchMedications(){
        showProgress(getString(R.string.fetch_progress));
        getMedicationsFromLocal();
    }

    private void showProgress(String title) {
        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage(title);
        mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(true);
        mProgress.setIndeterminate(true);
        mProgress.show();
    }

    private void dismissProgress(){
        if (mProgress != null){
            mProgress.dismiss();
        }
        mProgress = null;
    }

    private void setupListView(View view) {
        mAdapter = new MedicineListAdapter(getActivity(), this);

        //setup the recyclerview
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void OnClick(int position) {
        Intent intent = new Intent(getActivity(),AddOrEditMedicationActivity.class);
        intent.putExtra("MED", mAdapter.getItems(position));
        intent.putExtra("INDEX", position);
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 100 && resultCode == Activity.RESULT_OK) {
            if (data.hasExtra("MED")){
                Medicine medicine = (Medicine) data.getSerializableExtra("MED");
                int position = data.getIntExtra("INDEX",-1);
                mAdapter.editItem(position,medicine);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void configureReminders(ArrayList<Medicine> list){
        for(Medicine mMedication : list){
            ArrayList<String> time = mMedication.getTimings();
            ArrayList<Integer> newAlarmIds = new ArrayList<>();
            ArrayList<Integer> oldAlarmIds = new ArrayList<>();

            for(int i=0 ; i < time.size(); i ++){
                newAlarmIds.add(mUtils.getUniqueKey(mMedication.getId(), i));
            }

            HashMap<String,ArrayList<Integer>> mMedList = mApp.getPreference().getMedMap(mApp);
            if(mMedList != null  && mMedList.containsKey(mMedication.getId())){
                oldAlarmIds = mMedList.get(mMedication.getId());
            } else {
                mMedList = new HashMap<String,ArrayList<Integer>>();
            }

            mMedList.put(mMedication.getId(),newAlarmIds);
            mApp.getPreference().storeMedMap(mApp,mMedList);

            deleteAlarms(oldAlarmIds);
            updateAlarms(time,mMedication);
        }

        dismissProgress();
    }

    private void deleteAlarms(ArrayList<Integer> list){
        mUtils.removeAlarm(mApp,list);

        HashMap<Integer,Medicine> alarmList = mApp.getPreference().getAlarmMap(mApp);

        for (Integer id : list) {
            if(alarmList != null){
                alarmList.remove(id);
            }
        }
        mApp.getPreference().storeAlarmMap(mApp,alarmList);
    }

    private void updateAlarms(ArrayList<String> time, Medicine medicine){
        HashMap<Integer,Medicine> alarmList = mApp.getPreference().getAlarmMap(mApp);

        Medicine alarmMed = new Medicine();
        alarmMed.setId(medicine.getId());
        alarmMed.setName(medicine.getName());
        for(int i = 0 ; i < time.size(); i ++){
            int alarmId = mUtils.getUniqueKey(medicine.getId(), i);
            String[] timeList = TextUtils.split(mUtils.get24HourFormat(time.get(i)),":");
            mUtils.setAlarm(mApp,alarmId,timeList);
            alarmList.put(alarmId,alarmMed);
        }
        mApp.getPreference().storeAlarmMap(mApp,alarmList);
    }

    public void getMedicationsFromLocal() {
        final ArrayList<Medicine> medList =  new ArrayList<>();
        ServerAPI.getMedicationsFromLocal(mApp.getApplicationContext(), mUser.getId(), new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response) {
                if (response.statusCode == ServerAPI.STATUS_OK) {
                    try {
                        JSONArray medArray = new JSONArray(response.responseData);
                        for(int i = 0; i < medArray.length(); i++){
                            Medicine med = new Medicine();
                            JSONObject medObject = (JSONObject) medArray.get(i);
                            med.setId(medObject.getString("code"));
                            med.setName(medObject.getString("name"));
                            med.setDosage(medObject.getString("dosage"));
                            String[] list = medObject.getString("timing").split(",");
                            med.setTimings(new ArrayList<String>(Arrays.asList(list)));
                            medList.add(med);
                        }
                        if(medList.size() == 0){
                            fetchMedicationList();
                        } else {
                            dismissProgress();
                            mAdapter.addItems(medList);
                            mAdapter.notifyDataSetChanged();
                        }
                    }catch (JSONException e){
                        dismissProgress();
                    }
                } else {
                    // Show the error to the mUser
                    dismissProgress();
                }
            }
        });

    }

    private void fetchMedicationList(){
        final ArrayList<Medicine> medList =  new ArrayList<>();
        ServerAPI.getMedications(getActivity(), mUser.getId(), new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response) {
                if (response.statusCode == ServerAPI.STATUS_OK){
                    if(getActivity() == null){
                        return;
                    }
                    try {
                        ArrayList<String> uniqueCode = new ArrayList<>();

                        JSONObject jsonResponse = new JSONObject(response.responseData);
                        JSONArray entryArray = jsonResponse.getJSONArray("entry");
                        for(int i = 0; i < entryArray.length(); i++){
                            Medicine med = new Medicine();
                            JSONObject medObject = (JSONObject) entryArray.get(i);
                            JSONObject contObject = (JSONObject) medObject.getJSONObject("resource").getJSONArray("contained").get(0);
                            JSONObject codeObject = contObject.getJSONObject("code");
                            JSONObject codingObject = (JSONObject) codeObject.getJSONArray("coding").get(0);

                            String name = codingObject.getString("display");
                            String code = codingObject.getString("code");

                            med.setId(code);
                            med.setName(name.split("MG")[0] + "MG");
                            med.setDosage(getString(R.string.once_day));
                            ArrayList<String> list = new ArrayList<>();
                            list.add("9:00 AM");
                            med.setTimings(list);
                            if(!uniqueCode.contains(med.getId())){
                                medList.add(med);
                                uniqueCode.add(med.getId());
                            }
                        }

                        if (medList.size() > 0){
                            updateMedicationsToLocal(medList);
                            //configureReminders(medList);
                        }

                        mAdapter.addItems(medList);
                        dismissProgress();
                        mAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        dismissProgress();
                    }

                }
            }
        });
    }

    private void updateMedicationsToLocal(ArrayList<Medicine> list) {
        JSONObject data = formJSONForUpdateMedications(list);
        ServerAPI.updateMedicationsToLocal(getActivity(), data, new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response) {
                if (response.statusCode == ServerAPI.STATUS_OK) {
                    Log.d("IHI",response.responseData);
                }
            }
        });
    }

    private JSONObject formJSONForUpdateMedications(ArrayList<Medicine> list){
        JSONObject main =  new JSONObject();
        try {
            main.put("id",mUser.getId());
            JSONArray med_array = new JSONArray();
            for(int i = 0; i < list.size(); i++){
                Medicine medicine = list.get(i);
                JSONObject med_obj = new JSONObject();
                med_obj.put("code", medicine.getId());
                med_obj.put("name", medicine.getName());
                med_obj.put("dosage", medicine.getDosage());
                med_obj.put("timing", TextUtils.join(", ", medicine.getTimings()));
                med_array.put(med_obj);
            }
            main.put("list",med_array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return main;
    }
}
