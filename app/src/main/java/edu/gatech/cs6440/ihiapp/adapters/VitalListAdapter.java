package edu.gatech.cs6440.ihiapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.models.Vital;

/**
 * Created by arpithadudi on 4/21/18.
 */

public class VitalListAdapter extends RecyclerView.Adapter<VitalListAdapter.ViewHolder>{

    private Context mContext;
    private Map<String,Vital> mDataList;
    private String mType = "weight";

    public VitalListAdapter(Context context) {
        mContext = context;
        mDataList = new TreeMap<>();
    }

    public void addItems(TreeMap<String,Vital> list, String type) {
        mDataList.clear();
        mDataList.putAll(list);
        mType = type;
    }

    public void replaceItem(String date, String val){
        Vital vital = mDataList.get(date);
        if (vital == null){
            vital = new Vital();
        }
        if(mType.contains("Weight")){
            vital.setWeight(val);
        } else if (mType.contains("Rate")){
            vital.setHeartRate(val);
        }else if (mType.contains("Pressure")){
            vital.setBloodPressure(val);
        }else if (mType.contains("Sugar")){
            vital.setBloodSugar(val);
        }
        vital.setDate(date);
        mDataList.put(date,vital);
    }

    @Override
    public VitalListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.vital_list_item, parent, false);

        // Return a new holder instance
        return new VitalListAdapter.ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(VitalListAdapter.ViewHolder holder, final int position) {
        List<Vital> list = new ArrayList<>(mDataList.values());
        Vital vital = list.get(position);
        holder.mName.setText(dateFormatter(vital.getDate()));
        String value = "";
        if(mType.contains("Weight")){
            value = vital.getWeight();
        } else if (mType.contains("Rate")){
            value = vital.getHeartRate();
        }else if (mType.contains("Pressure")){
            value = vital.getBloodPressure();
        }else if (mType.contains("Sugar")){
            value = vital.getBloodSugar();
        }
        holder.mValue.setText(value);
    }

    public String dateFormatter(String currentDate) {
        String dateFormat = "MMM d";
        String date = currentDate;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date newDate  = format.parse(currentDate);
            format = new SimpleDateFormat(dateFormat);
            date = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mName;
        private TextView mValue;

        public ViewHolder(final View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.date_view);
            mValue = itemView.findViewById(R.id.value_view);
        }
    }
}
