package edu.gatech.cs6440.ihiapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.User;
import edu.gatech.cs6440.ihiapp.server.ServerAPI;
import edu.gatech.cs6440.ihiapp.server.ServerResponseCallback;
import edu.gatech.cs6440.ihiapp.server.ServerResponseData;

/**
 * A login screen that offers login via id/password.
 */
public class SignInActivity extends BaseActivity {

    // UI references.
    private EditText mPatientIDView;
    private EditText mPasswordView;
    private Button mSignInBtn;
    private TextView mSignUpBtn;

    //Variables
    private IHIApp mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mApp = (IHIApp) getApplicationContext();

        // Set up the login form.
        mPatientIDView = findViewById(R.id.id_view);
        mPasswordView = findViewById(R.id.password_view);
        mSignInBtn = findViewById(R.id.btn_signin);
        mSignUpBtn = findViewById(R.id.sign_up_button);

        // Callbacks
        mSignInBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mSignUpBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });

    }

    /**
     * Attempts to sign in specified by the login form.
     * If there are form errors (invalid password, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mPatientIDView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String patientID = mPatientIDView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(patientID)) {
            mPatientIDView.setError(getString(R.string.error_field_required));
            focusView = mPatientIDView;
            cancel = true;
        } else if (!isPatientIDValid(patientID)) {
            mPatientIDView.setError(getString(R.string.error_invalid_id));
            focusView = mPatientIDView;
            cancel = true;
        }else if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            showProgress();
            checkUserAccount(patientID,password);
        }
    }

    public void checkUserAccount(String id, String  password) {
        ServerAPI.login(mApp.getApplicationContext(), id, password, new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response  ) {
                if (response.statusCode == ServerAPI.STATUS_OK) {

                    try {
                        JSONObject responseData = new JSONObject(response.responseData);
                        if(responseData.has("message")){
                            //Show an error
                            dismissProgress();
                            showErrorToast(SignInActivity.this,responseData.getString("message"));
                        } else {
                            //Get the user object
                            User user = new Gson().fromJson(response.responseData, User.class);

                            //save the user
                            mApp.getPreference().storeUser(SignInActivity.this,user);
                            dismissProgress();

                            //move to the Home screen
                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    //Show an error
                    dismissProgress();
                    showErrorToast(SignInActivity.this,getString(R.string.auth_error));
                }
            }
        });
    }

    private boolean isPatientIDValid(String id) {
        //TODO: Replace this with your own logic
        return id.length() > 5;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 6;
    }
}

