package edu.gatech.cs6440.ihiapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.models.Medicine;

/**
 * Created by arpithadudi on 4/19/18.
 */
public class MedicineListAdapter extends RecyclerView.Adapter<MedicineListAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Medicine> mDataList;
    private MedicineListener mListener;

    public MedicineListAdapter(Context context, MedicineListener listener) {
        mContext = context;
        mDataList = new ArrayList<Medicine>();
        mListener = listener;
    }

    public void addItems(ArrayList<Medicine> list) {
        mDataList.clear();
        mDataList.addAll(list);
    }

    public Medicine editItem(int index, Medicine med) {
        return mDataList.set(index, med);
    }

    public Medicine getItems(int index) {
        return mDataList.get(index);
    }

    public ArrayList<Medicine> getItems() {
        return mDataList;
    }

    @Override
    public MedicineListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.medicine_list_item, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(MedicineListAdapter.ViewHolder holder, final int position) {
        Medicine med = mDataList.get(position);
        holder.mName.setText(med.getName());
        holder.mDosage.setText(med.getDosage());

        holder.mEditBtn.setTag(position);
        holder.mEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = (int) view.getTag();
                mListener.OnClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mName;
        private TextView mDosage;
        private ImageButton mEditBtn;

        public ViewHolder(final View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.name_view);
            mDosage = itemView.findViewById(R.id.timing_view);
            mEditBtn = itemView.findViewById(R.id.btn_edit);
        }
    }

    public interface MedicineListener {
        void OnClick(int position);
    }
}


