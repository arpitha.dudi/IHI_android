package edu.gatech.cs6440.ihiapp.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.activities.AddVitalActivity;
import edu.gatech.cs6440.ihiapp.adapters.VitalListAdapter;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.User;
import edu.gatech.cs6440.ihiapp.models.Vital;
import edu.gatech.cs6440.ihiapp.server.ServerAPI;
import edu.gatech.cs6440.ihiapp.server.ServerResponseCallback;
import edu.gatech.cs6440.ihiapp.server.ServerResponseData;

/**
 * Created by arpithadudi on 4/21/18.
 */

public class VitalFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    // UI references.
    private LineChart mVitalChart;
    private ProgressDialog mProgress;
    private RecyclerView mRecyclerView;
    private VitalListAdapter mAdapter;
    private Spinner mVitalTypeSpinner;
    private FloatingActionButton mAddBtn;

    //Variables
    private IHIApp mApp;
    private User mUser;
    private String mType = "Weight";
    private TreeMap<String,Vital> mVitalList = new TreeMap<>();

    //test
    private Map<String, String> mGraph_map;
    private ArrayList<String> mDateRange;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.vital_fragment, container, false);

        mApp = (IHIApp) getActivity().getApplicationContext();
        mUser = mApp.getPreference().getUser(getActivity());

        mAddBtn = view.findViewById(R.id.add_fab);
        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),AddVitalActivity.class);
                intent.putExtra("TYPE", mType);
                startActivityForResult(intent, 100);
            }
        });

        setupChartView(view);
        setupListView(view);
        setupSpinner(view);
        setIntialData();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0 ||dy<0 && mAddBtn.isShown())
                    mAddBtn.hide();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                if (newState == RecyclerView.SCROLL_STATE_IDLE){
                    mAddBtn.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        showProgress();
        fetchVitalsList();
        return view;
    }

    private void setupChartView(View view) {
        mVitalChart = view.findViewById(R.id.vital_chart);

        mVitalChart.setDrawGridBackground(false);
        mVitalChart.getLegend().setEnabled(false);
        mVitalChart.setDescription("");
        mVitalChart.setNoDataTextDescription("No Data");
        mVitalChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mVitalChart.getAxisRight().setEnabled(false);

        YAxis leftAxis = mVitalChart.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.setAxisMaxValue(120f);
        leftAxis.setAxisMinValue(0f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);
        mVitalChart.invalidate();
    }

    private void setTheYRange(int max, int min) {
        mVitalChart.getAxisLeft().setAxisMaxValue(max);
        mVitalChart.getAxisLeft().setAxisMinValue(min);
    }

    private void setupListView(View view) {
        mAdapter = new VitalListAdapter(getActivity());

        //setup the recycler view
        mRecyclerView = view.findViewById(R.id.vital_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupSpinner(View view) {
        mVitalTypeSpinner = view.findViewById(R.id.type);
        final String[] list = getResources().getStringArray(R.array.vital_array);

        mVitalTypeSpinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> array_list = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
        array_list.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mVitalTypeSpinner.setAdapter(array_list);
    }

    private ArrayList<String> setXAxisValues() {
        if (mDateRange == null){
            mDateRange = getRangeofDate();
        }
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < mDateRange.size(); i++) {
            xVals.add(mDateRange.get(i));
        }
        return xVals;
    }

    private ArrayList<Entry> setYAxisValues() {
        ArrayList<String> datesRange = getRangeofDate1();

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        if(mGraph_map != null){
            for (int i = 0 ; i < datesRange.size(); i ++){
                String val = mGraph_map.get(datesRange.get(i));
                if (val == null){
                    yVals.add(new Entry(-1f,i));
                } else {
                    yVals.add(new Entry(Float.parseFloat(val), i));
                }
            }
        } else {
            for (int i = 0 ; i < datesRange.size(); i ++){
                yVals.add(new Entry(-1f,i));
            }
        }

        return yVals;
    }

    private void setIntialData() {
        ArrayList<String> xVals = setXAxisValues();
        ArrayList<Entry> yVals = setYAxisValues();

        LineDataSet set = new LineDataSet(yVals, "");
        set.setFillAlpha(100);
        set.setFillColor(getResources().getColor(R.color.colorPrimary));
        set.setColor(Color.BLACK);
        set.setCircleColor(Color.BLACK);
        set.setLineWidth(1f);
        set.setCircleRadius(3f);
        set.setDrawCircleHole(false);
        set.setValueTextSize(9f);
        set.setDrawFilled(true);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set);

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);

        // set data
        mVitalChart.setData(data);
        mVitalChart.notifyDataSetChanged(); // let the chart know it's data changed
        mVitalChart.invalidate();
    }

    private void refreshGraph() {
        LineData data1 =  mVitalChart.getLineData();
        ArrayList<ILineDataSet> dataSets1 = (ArrayList<ILineDataSet>) data1.getDataSets();
        dataSets1.remove(0);

        ArrayList<Entry> yVals = setYAxisValues();
        LineDataSet set1 = new LineDataSet(yVals, "");
        set1.setFillAlpha(100);
        set1.setFillColor(getResources().getColor(R.color.colorPrimary));
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);

        dataSets1.add(set1);
        data1.notifyDataChanged();
        mVitalChart.notifyDataSetChanged(); // let the chart know it's data changed
        mVitalChart.invalidate();
    }

    private void showProgress() {
        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Fetching your vitals");
        mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(true);
        mProgress.setIndeterminate(true);
        mProgress.show();
    }

    private void dismissProgress() {
        if (mProgress != null) {
            mProgress.dismiss();
        }
        mProgress = null;
    }

//    private void testData() {
//        //To set's of data
//        date = getRangeofDate();
//
//        // create a hashmap  for the graph
//        // date -> value
//        mGraph_map = new TreeMap<>();
//        for (int i = 0; i < 7; i++) {
//            mGraph_map.put(date.get(i), val[i]);
//        }
//
//        //create a hashmap for the adapter
//        // date -> vital sign
//        TreeMap<String, Vital> map_vitals = new TreeMap<>();
//        for (int i = 0; i < 7; i++) {
//            Vital vital = new Vital();
//            vital.setDate(date.get(i));
//            //vital.setValue(val[i]);
//            map_vitals.put(date.get(i), vital);
//        }
//
//        refreshGraph();
//        mAdapter.addItems(map_vitals);
//        mAdapter.notifyDataSetChanged();
//    }

    private void parseData(){
        ArrayList<String> datesRange = getRangeofDate1();
        mGraph_map = new TreeMap<>();
        for (int i = 0; i < datesRange.size(); i++){
            Vital vital = mVitalList.get(datesRange.get(i));
            String value = "0";
            if (vital != null){
                if(mType.contains("Weight")){
                    value = vital.getWeight();
                } else if (mType.contains("Rate")){
                    value = vital.getHeartRate();
                }else if (mType.contains("Pressure")){
                    value = vital.getBloodPressure();
                }else if (mType.contains("Sugar")){
                    value = vital.getBloodSugar();
                }
            }
            mGraph_map.put(datesRange.get(i), value);
//            setTheYRange(Math.max(Integer.valueOf(Collections.max(mGraph_map.values())),120), 0);
            refreshGraph();

            mAdapter.addItems(mVitalList, mType);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void fetchVitalsList() {
        mVitalList.clear();
        ServerAPI.getVitals(getActivity(), mUser.getId(), new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response) {
                if (response.statusCode == ServerAPI.STATUS_OK) {
                    try {
                        JSONArray vitalsArray = new JSONArray(response.responseData);
                        Gson gson = new Gson();
                        for(int i = 0; i < vitalsArray.length(); i++){
                            Vital vital = gson.fromJson(String.valueOf(vitalsArray.get(i)), Vital.class);
                            mVitalList.put(vital.getDate(),vital);
                        }
                        parseData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dismissProgress();
                }
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mType = mVitalTypeSpinner.getSelectedItem().toString();
        parseData();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public ArrayList<String> getRangeofDate() {
        String dateFormat = "MMM d";
        ArrayList<String> dates = new ArrayList<>();
        for (int i = 0; i <=7 ; i ++) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat s = new SimpleDateFormat(dateFormat);
            cal.roll(Calendar.DAY_OF_YEAR, -i);
            dates.add(s.format(new Date(cal.getTimeInMillis())));
        }
        return dates;
    }

    public ArrayList<String> getRangeofDate1() {
        String dateFormat = "yyyy-MM-dd";
        ArrayList<String> dates = new ArrayList<>();
        for (int i = 0; i <=7 ; i ++) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat s = new SimpleDateFormat(dateFormat);
            cal.roll(Calendar.DAY_OF_YEAR, -i);
            dates.add(s.format(new Date(cal.getTimeInMillis())));
        }
        return dates;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 100 && resultCode == Activity.RESULT_OK) {
            if (data.hasExtra("VALUE")){
                String value = data.getStringExtra("VALUE");
                String date = data.getStringExtra("DATE");
                if(mGraph_map.containsKey(date)){
                    mGraph_map.put(date,value);
                    refreshGraph();

                    Vital vital = mVitalList.get(date);
                    if(vital == null){
                        vital = new Vital();
                        vital.setDate(date);
                    }
                    if(mType.contains("Weight")){
                        vital.setWeight(value);
                    } else if (mType.contains("Rate")){
                        vital.setHeartRate(value);
                    }else if (mType.contains("Pressure")){
                        vital.setBloodPressure(value);
                    }else if (mType.contains("Sugar")){
                        vital.setBloodSugar(value);
                    }
                    mVitalList.put(vital.getDate(),vital);
                }
                mAdapter.replaceItem(date,value);
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
