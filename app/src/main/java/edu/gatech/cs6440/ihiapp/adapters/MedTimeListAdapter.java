package edu.gatech.cs6440.ihiapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.models.Medicine;

/**
 * Created by arpithadudi on 4/20/18.
 */

public class MedTimeListAdapter extends RecyclerView.Adapter<MedTimeListAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<String> mData;
    private MedTimeListAdapter.TimeListener mListener;

    public MedTimeListAdapter(Context context, MedTimeListAdapter.TimeListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void addItem(ArrayList<String> med) {
        mData = med;
    }

    public void editItem(int index, String val){
        mData.set(index,val);
    }

    @Override
    public MedTimeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.medtime_list_item, parent, false);

        // Return a new holder instance
        return new MedTimeListAdapter.ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(MedTimeListAdapter.ViewHolder holder, final int position) {

        holder.mTime.setText(mData.get(position));
        holder.mOrder.setText("Take " + (position + 1));

        holder.mTime.setTag(position);
        holder.mTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = (int) view.getTag();
                mListener.OnClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTime;
        private TextView mOrder;

        public ViewHolder(final View itemView) {
            super(itemView);
            mTime = itemView.findViewById(R.id.time_view);
            mOrder = itemView.findViewById(R.id.dosage_view);
        }
    }

    public interface TimeListener {
        void OnClick(int position);
    }
}
