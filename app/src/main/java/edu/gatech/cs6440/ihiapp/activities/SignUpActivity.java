package edu.gatech.cs6440.ihiapp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import edu.gatech.cs6440.ihiapp.R;
import edu.gatech.cs6440.ihiapp.adapters.DoctorListAdapter;
import edu.gatech.cs6440.ihiapp.application.IHIApp;
import edu.gatech.cs6440.ihiapp.models.Medicine;
import edu.gatech.cs6440.ihiapp.models.User;
import edu.gatech.cs6440.ihiapp.server.ServerAPI;
import edu.gatech.cs6440.ihiapp.server.ServerResponseCallback;
import edu.gatech.cs6440.ihiapp.server.ServerResponseData;

/**
 * A create account screen that offers login
 * via id, name, email, number, password.
 */
public class SignUpActivity extends BaseActivity implements DoctorListAdapter.DoctorListAdapterListener {

    //Variables
    private IHIApp mApp;
    private User mDocSelected;
    private DoctorListAdapter mDoctorAdapter;

    // UI references.
    private EditText mPatientIDView;
    private EditText mEmailView;
    private EditText mNumberView;
    private EditText mNameView;
    private EditText mPasswordView;
    private EditText mDoctorView;
    private Button mSignUpBtn;
    private Button mDocSearchBtn;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mApp = (IHIApp) getApplicationContext();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // Set up the signIn form.
        mPatientIDView = findViewById(R.id.id_view);
        mNameView = findViewById(R.id.name_view);
        mNumberView = findViewById(R.id.number_view);
        mEmailView = findViewById(R.id.email_view);
        mPasswordView = findViewById(R.id.password_view);
        mDoctorView = findViewById(R.id.doctor_view);
        mDocSearchBtn = findViewById(R.id.btn_search);
        mSignUpBtn = findViewById(R.id.btn_signup);

        mSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignUp();
            }
        });

        mDocSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doctorDialog();
            }
        });

        getListofDoctors();
    }

    private void getListofDoctors(){
        mDoctorAdapter = new DoctorListAdapter(SignUpActivity.this,SignUpActivity.this);
        getDoctorList();
    }

    private void doctorDialog(){
        android.app.AlertDialog.Builder builder;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            builder = new android.app.AlertDialog.Builder(SignUpActivity.this, R.style.AppCompatAlertDialogStyle);
        }else{
            builder = new android.app.AlertDialog.Builder(SignUpActivity.this);
        }
        builder.setTitle(getString(R.string.doctor_hint));

        //setup the recyclerview
        final RecyclerView associateRecyclerView = new RecyclerView(mApp);
        associateRecyclerView.setLayoutManager(new LinearLayoutManager(SignUpActivity.this));
        associateRecyclerView.setAdapter(mDoctorAdapter);
        builder.setView(associateRecyclerView);

        builder.setPositiveButton(getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (mDocSelected != null){
                    mDoctorView.setText(mDocSelected.getName());
                }
            }
        })
        .setNegativeButton(getString(R.string.cancel), null);
        builder.show();
    }

    /**
     * Attempts to register the account specified by the  form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptSignUp() {

        // Reset errors.
        mPatientIDView.setError(null);
        mNameView.setError(null);
        mNumberView.setError(null);
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String patientID = mPatientIDView.getText().toString();
        String name = mNameView.getText().toString();
        String email = mEmailView.getText().toString();
        String number = mNumberView.getText().toString();
        String password = mPasswordView.getText().toString();
        String doctorName = mDoctorView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(patientID)) {
            mPatientIDView.setError(getString(R.string.error_field_required));
            focusView = mPatientIDView;
            cancel = true;
        } else if (!isPatientIDValid(patientID)) {
            mPatientIDView.setError(getString(R.string.error_invalid_id));
            focusView = mPatientIDView;
            cancel = true;
        } else if (TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        } else if (TextUtils.isEmpty(number)) {
            mNumberView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        } else if (!isNumberValid(number)) {
            mNumberView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        } else if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        } else if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }else if (TextUtils.isEmpty(doctorName)) {
            mDoctorView.setError(getString(R.string.error_field_required));
            focusView = mDoctorView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            showProgress();
            User user = new User();
            user.setId(patientID);
            user.setName(name);
            user.setEmail(email);
            user.setNumber(number);
            user.setPassword(password);

            createUserAccount(user,mDocSelected.getId());
        }
    }

    public void createUserAccount(final User user, final String docId) {
        ServerAPI.createAccount(mApp.getApplicationContext(), user, docId, new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response) {
                if (response.statusCode == ServerAPI.STATUS_OK) {
                    try {
                        JSONObject responseData = new JSONObject(response.responseData);
                        String message = responseData.getString("message");
                        if (message.contains("successfully")){
                            mApp.getPreference().storeUser(SignUpActivity.this,user);
                            mApp.getPreference().storeDoctor(SignUpActivity.this,docId);

                            dismissProgress();
                            Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            dismissProgress();
                            showErrorToast(SignUpActivity.this,message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dismissProgress();
                        showErrorToast(SignUpActivity.this,getString(R.string.auth_error));
                    }


                } else {
                    // Show the error to the user
                    dismissProgress();
                    showErrorToast(SignUpActivity.this,getString(R.string.auth_error));
                }
            }
        });
    }

    public void getDoctorList() {
        final ArrayList<User> docList =  new ArrayList<>();

        ServerAPI.getDoctorsList(mApp.getApplicationContext(), new ServerResponseCallback() {
            @Override
            public void onResponse(ServerResponseData response) {
                if (response.statusCode == ServerAPI.STATUS_OK) {
                    try {
                        JSONArray docArray = new JSONArray(response.responseData);
                        for(int i = 0; i < docArray.length(); i++){
                            User user = new User();
                            JSONObject userObject = (JSONObject) docArray.get(i);
                            user.setId(userObject.getString("id"));
                            user.setName(userObject.getString("name"));
                            docList.add(user);
                        }
                        mDoctorAdapter.addItems(docList);
                        mDoctorAdapter.notifyDataSetChanged();
                    }catch (JSONException e){
                        dismissProgress();
                        showErrorToast(SignUpActivity.this,getString(R.string.server_error));
                    }
                } else {
                    // Show the error to the user
                    dismissProgress();
                    showErrorToast(SignUpActivity.this,getString(R.string.server_error));
                }
            }
        });

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPatientIDValid(String id) {
        //TODO: Replace this with your own logic
        return id.length() > 5;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 6;
    }

    private boolean isNumberValid(String number) {
        //TODO: Replace this with your own logic
        return number.length() == 10;
    }

    @Override
    public void OnCheckClick(User user) {
        mDocSelected = user;
    }
}
